@extends('template.v_template')
@section('title', 'Create Pertanyaan')

@section('content')
<form action="/pertanyaan/store" method="post">
    @csrf
<div class="card">
    <div class="card-body">
        <form action="/pertanyaan/store" method="post">
            <div class="container">
            <div class="col-6">
                <div class="row">
                    <label for="judul" class="form-label">Judul</label>
                    <input type="text" name="judul" class="form-control" id="judul" value="{{ old('judul','') }}" required>
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
            </div> <br>
            <div class="col-6">
                <div class="row">
                    <label for="isi" class="form-label">Isi</label><br>
                    <textarea name="isi" class="form-control" id="isi" rows="3" required>{{ old('isi','') }}</textarea>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
            </div> <br>
                      
 
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection
