@extends('template.v_template')
@section('title', 'Detail Pertanyaan')

@section('content')
    <?php $no=1; ?>
    
        <table class="table table-bordered">
            <tr>
                <th>ID</th>
                <th>Judul</th>
                <th>Isi</th>
                <th>Tanggal Dibuat</th>
            </tr>    
            <tr>
                <td>{{ $questions->id }}</td>
                <td>{{ $questions->judul }}</td>
                <td>{{ $questions->isi }}</td>
                <td>{{ $questions->tanggal_dibuat }}</td>
            </tr>
        </table>
        <br>
        <a href="/pertanyaan" class="btn btn-success">Kembali</a>
        
    
@endsection