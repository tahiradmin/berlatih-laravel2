@extends('template.v_template')
@section('title', 'Pertanyaan')
@push('style')
    <link rel="stylesheet" href="{{ asset('template') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
    
    <a href="/pertanyaan/create" class="btn btn-primary">Create Pertanyaan</a>
    <br> <br>
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">List Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Judul</th>
              <th>Isi Pertanyaan</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach ($questions as $question)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $question->judul }}</td>
                    <td>{{ $question->isi }}</td>
                    <td>
                      <div class="row">
                      <a href="/pertanyaan/{{ $question->id }}" class="btn btn-sm btn-primary">Detail</a> &nbsp;
                      <a href="/pertanyaan/{{ $question->id }}/edit" class="btn btn-sm btn-warning">Edit</a>&nbsp;
                      <form action="/pertanyaan/{{ $question->id }}" method="post">
                        @csrf
                        @method('DELETE')
                      <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                    </form>
                  </div>
                    </td>
                  </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>isi Pertanyaan</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    
      @endsection
      @push('script')
      <script src="{{ asset('template') }}/plugins/datatables/jquery.dataTables.js"></script>
      <script src="{{ asset('template') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
      <script>
        $(function () {
          $("#example1").DataTable();
        });
      </script>
      @endpush
