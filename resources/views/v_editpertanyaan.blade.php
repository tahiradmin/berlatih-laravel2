@extends('template.v_template')
@section('title', 'Edit Pertanyaan')
@section('content')
<form action="/pertanyaan/{{ $questions->id }}" method="post">
    @csrf
    @method('put')
<div class="card">
    <div class="card-body">
            
                <div class="container">
                
                    <div class="col-6">
                        <div class="row">
                            <label for="judul" class="form-label">Judul</label>
                            <input type="text" name="judul" class="form-control" id="judul" value="{{ $questions->judul }}">
                            @error('judul')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div><br>
                    <div class="col-6">
                        <div class="row">
                            <label for="isi" class="form-label">Isi</label>
                            <textarea name="isi" class="form-control" id="isi" rows="3">{{ $questions->isi }}</textarea>
                            @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                    </div> <br>
                
                <button type="submit" class="btn btn-primary">Update</button>
           
        </form>
    </div>
</div>
@endsection
